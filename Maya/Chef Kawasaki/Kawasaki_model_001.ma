//Maya ASCII 2018ff08 scene
//Name: Kawasaki_model_001.ma
//Last modified: Fri, Aug 03, 2018 12:59:28 AM
//Codeset: 1252
requires maya "2018ff08";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201804211841-f3d65dda2a";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "6C8050A7-4FC6-1A82-17D3-C2A0DD892B2D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.3113844946496132 3.4312591664293066 3.9240891362168715 ;
	setAttr ".r" -type "double3" -41.738352729677231 -26.999999999990163 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "29EDE176-4B02-0634-C690-FC923139E2BE";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 5.755611689866555;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "61D0C322-4765-5C26-9243-2390CD6FF72B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "D7071713-49F3-152A-62BF-B5B16D341DD3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "D6F63BC2-4FD7-9DD4-6725-FC8EE4B4DEBB";
	setAttr ".t" -type "double3" 0.26793596432318167 0.92871970625336253 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "A3C4C3E5-4C1E-A222-34DE-7484307E4C56";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4.1179522732865914;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "02BA4628-4912-6F37-D6A3-9EB226417B63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "4446E5C1-4EDF-C8D3-FE89-73A56114008E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "imagePlane1";
	rename -uid "04D83709-4751-4491-70F6-83BDBC870B62";
	setAttr ".t" -type "double3" -0.56310331585431084 0.15155128834149609 -0.95509414624502931 ;
	setAttr ".r" -type "double3" 0 0 4.6492017692847591 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "A35732B0-400D-00A5-5356-F2874FFB5EF7";
	setAttr -k off ".v";
	setAttr ".fc" 204;
	setAttr ".imn" -type "string" "C:/Users/jonat/Downloads/ChefKawasaki.png";
	setAttr ".cov" -type "short2" 454 420 ;
	setAttr ".dlc" no;
	setAttr ".w" 4.54;
	setAttr ".h" 4.2;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCylinder1";
	rename -uid "8D5257FC-4860-7D7D-8AD1-93804F58AE68";
	setAttr ".r" -type "double3" 0 9 0 ;
	setAttr ".s" -type "double3" 1 1.6999999752404025 1 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "C3812940-4826-8F42-AF22-15B260EB0483";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.54374980926513672 0.56725227832794189 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[452:459]" -type "float3"  -0.010957949 0.046062604 
		0.069185793 -0.010957949 0.046062604 0.069185793 -0.0065138843 0 0.041127048 -0.0065138843 
		0 0.041127048 -0.010957949 0.046062604 0.069185793 -0.0065138843 0 0.041127048 -0.010957949 
		0.046062604 0.069185793 -0.0065138843 0 0.041127048;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder2";
	rename -uid "D738BABF-4A3B-8CC2-7E7C-9BA532D71DB9";
	setAttr ".t" -type "double3" 0.14023852756181071 1.6765711932058311 -0.2933694425771734 ;
	setAttr ".r" -type "double3" -25.218932006187437 1.9878466759146985e-16 -8.30721745359042 ;
	setAttr ".s" -type "double3" 0.33977989275896631 0.21141859472516614 0.33977989275896631 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "A109E9A2-4506-CECC-B2CA-D8BD8819B3F4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002980232239 0.84375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 25 ".pt";
	setAttr ".pt[43]" -type "float3" -0.00053951045 -0.28266272 0.0015752133 ;
	setAttr ".pt[44]" -type "float3" -0.18627776 -0.28266266 0.25722197 ;
	setAttr ".pt[49]" -type "float3" 0.25510707 -0.28266272 0.18731394 ;
	setAttr ".pt[54]" -type "float3" 0.18519911 -0.28266266 -0.25407228 ;
	setAttr ".pt[59]" -type "float3" -0.25618616 -0.2826632 -0.18416362 ;
	setAttr ".pt[62]" -type "float3" 0.55817479 1.0631653e-07 0.76826191 ;
	setAttr ".pt[63]" -type "float3" 0.29344988 -1.5775596e-07 0.90314537 ;
	setAttr ".pt[64]" -type "float3" 2.506151e-07 -1.5775596e-07 0.94962442 ;
	setAttr ".pt[65]" -type "float3" -0.29344976 1.0631653e-07 0.90314519 ;
	setAttr ".pt[66]" -type "float3" -0.16192053 0.005375375 0.22369729 ;
	setAttr ".pt[67]" -type "float3" -0.76826227 -3.6374836e-08 0.55817497 ;
	setAttr ".pt[68]" -type "float3" -0.90314507 -1.5775596e-07 0.29345056 ;
	setAttr ".pt[69]" -type "float3" -0.94962418 -9.6783577e-09 3.2116122e-07 ;
	setAttr ".pt[70]" -type "float3" -0.90314507 1.0631653e-07 -0.29345089 ;
	setAttr ".pt[71]" -type "float3" -0.22266161 0.0053751613 -0.15980631 ;
	setAttr ".pt[72]" -type "float3" -0.55817443 -3.6374836e-08 -0.76826245 ;
	setAttr ".pt[73]" -type "float3" -0.29344985 -3.6374836e-08 -0.90314519 ;
	setAttr ".pt[74]" -type "float3" 2.1083501e-07 1.0631653e-07 -0.94962442 ;
	setAttr ".pt[75]" -type "float3" 0.29345 -3.6374836e-08 -0.90314507 ;
	setAttr ".pt[76]" -type "float3" 0.16084176 0.0053752633 -0.22054756 ;
	setAttr ".pt[77]" -type "float3" 0.76826245 1.0631653e-07 -0.55817437 ;
	setAttr ".pt[78]" -type "float3" 0.90314561 -3.6374836e-08 -0.2934511 ;
	setAttr ".pt[79]" -type "float3" 0.94962424 -2.774793e-07 -2.3307196e-06 ;
	setAttr ".pt[80]" -type "float3" 0.90314502 -9.6783577e-09 0.29345074 ;
	setAttr ".pt[81]" -type "float3" 0.22158401 0.0053751329 0.16295572 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "0AA6C330-49F9-BFC9-6791-B58F9F414579";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "10CA3EE4-4AD1-0EB0-B22B-798466B302DC";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "BE35DD44-4AB2-EA49-598A-2286C742A1FC";
createNode displayLayerManager -n "layerManager";
	rename -uid "F8785603-4C6E-27A4-E7FC-FEA40D6A46AD";
createNode displayLayer -n "defaultLayer";
	rename -uid "C9ADED31-4C91-E0CA-E5A5-96BDA0D8DB33";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "12BFBDD2-4BE5-0DD9-F507-66ADB5D86F4A";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "6BB871D2-4B2F-C530-C4A8-AAA83DB192AF";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "43104DDD-4902-2E3A-B2D4-90B5647837B5";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "7B44B98D-4C13-5C74-1B96-AEB5C39BF9D8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.057871006429195404;
	setAttr ".re" 55;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "86691935-4AA3-E1FD-DA33-87A9BC154418";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[100:101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]" "e[113]" "e[115]" "e[117]" "e[119]" "e[121]" "e[123]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.09218142181634903;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "B7F56277-4F80-7305-A82C-A1832CB6B261";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[140:141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[175]" "e[177]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.12000375986099243;
	setAttr ".re" 140;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "FB981406-4483-280E-ACBD-0392C016C989";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[180:181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[211]" "e[213]" "e[215]" "e[217]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.14948084950447083;
	setAttr ".re" 180;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "9606F878-4F5C-CF1B-1A6D-ACA591BD2E7B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[220:221]" "e[223]" "e[225]" "e[227]" "e[229]" "e[231]" "e[233]" "e[235]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.28983744978904724;
	setAttr ".re" 220;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "97966261-458E-CB6E-8F62-42A45E1E22BD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[260:261]" "e[263]" "e[265]" "e[267]" "e[269]" "e[271]" "e[273]" "e[275]" "e[277]" "e[279]" "e[281]" "e[283]" "e[285]" "e[287]" "e[289]" "e[291]" "e[293]" "e[295]" "e[297]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.29089993238449097;
	setAttr ".re" 260;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "1A0367FD-479F-8711-492E-BA99C2677CE8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[300:301]" "e[303]" "e[305]" "e[307]" "e[309]" "e[311]" "e[313]" "e[315]" "e[317]" "e[319]" "e[321]" "e[323]" "e[325]" "e[327]" "e[329]" "e[331]" "e[333]" "e[335]" "e[337]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.47146779298782349;
	setAttr ".re" 300;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "651D1E53-449B-2807-70B9-DF9FE7C1B90C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[340:341]" "e[343]" "e[345]" "e[347]" "e[349]" "e[351]" "e[353]" "e[355]" "e[357]" "e[359]" "e[361]" "e[363]" "e[365]" "e[367]" "e[369]" "e[371]" "e[373]" "e[375]" "e[377]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.30120575428009033;
	setAttr ".re" 340;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "FA8CB738-4209-34DE-D930-64A5E32AA6E3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[380:381]" "e[383]" "e[385]" "e[387]" "e[389]" "e[391]" "e[393]" "e[395]" "e[397]" "e[399]" "e[401]" "e[403]" "e[405]" "e[407]" "e[409]" "e[411]" "e[413]" "e[415]" "e[417]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.4476146399974823;
	setAttr ".re" 380;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "655BED25-479B-B345-7F54-F0B7671CF049";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[420:421]" "e[423]" "e[425]" "e[427]" "e[429]" "e[431]" "e[433]" "e[435]" "e[437]" "e[439]" "e[441]" "e[443]" "e[445]" "e[447]" "e[449]" "e[451]" "e[453]" "e[455]" "e[457]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.36014711856842041;
	setAttr ".re" 420;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "B4BAC4F7-4A17-D0F7-B3A0-B7835C587380";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[460:461]" "e[463]" "e[465]" "e[467]" "e[469]" "e[471]" "e[473]" "e[475]" "e[477]" "e[479]" "e[481]" "e[483]" "e[485]" "e[487]" "e[489]" "e[491]" "e[493]" "e[495]" "e[497]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.53877604007720947;
	setAttr ".re" 465;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "DD940943-4805-434A-427D-67AEF1846A03";
	setAttr ".uopa" yes;
	setAttr -s 240 ".tk";
	setAttr ".tk[0]" -type "float3" -0.80783868 0 0.26248235 ;
	setAttr ".tk[1]" -type "float3" -0.68718827 0 0.49927166 ;
	setAttr ".tk[2]" -type "float3" -0.49927187 0 0.68718791 ;
	setAttr ".tk[3]" -type "float3" -0.26248261 0 0.80783826 ;
	setAttr ".tk[4]" -type "float3" -1.012576e-07 0 0.84941053 ;
	setAttr ".tk[5]" -type "float3" 0.26248237 0 0.8078382 ;
	setAttr ".tk[6]" -type "float3" 0.4992716 0 0.68718773 ;
	setAttr ".tk[7]" -type "float3" 0.68718773 0 0.49927151 ;
	setAttr ".tk[8]" -type "float3" 0.80783808 0 0.26248223 ;
	setAttr ".tk[9]" -type "float3" 0.84941041 0 -1.5188664e-07 ;
	setAttr ".tk[10]" -type "float3" 0.80783808 0 -0.26248258 ;
	setAttr ".tk[11]" -type "float3" 0.68718767 0 -0.49927178 ;
	setAttr ".tk[12]" -type "float3" 0.49927151 0 -0.68718791 ;
	setAttr ".tk[13]" -type "float3" 0.26248229 0 -0.80783826 ;
	setAttr ".tk[14]" -type "float3" -7.5943312e-08 0 -0.84941053 ;
	setAttr ".tk[15]" -type "float3" -0.26248243 0 -0.8078382 ;
	setAttr ".tk[16]" -type "float3" -0.4992716 0 -0.68718785 ;
	setAttr ".tk[17]" -type "float3" -0.68718773 0 -0.49927172 ;
	setAttr ".tk[18]" -type "float3" -0.80783808 0 -0.26248252 ;
	setAttr ".tk[19]" -type "float3" -0.84941041 0 -1.5188664e-07 ;
	setAttr ".tk[20]" -type "float3" -0.88456029 0 0.28741112 ;
	setAttr ".tk[21]" -type "float3" -0.75245237 0 0.54668844 ;
	setAttr ".tk[22]" -type "float3" -0.54668874 0 0.75245202 ;
	setAttr ".tk[23]" -type "float3" -0.28741139 0 0.88455987 ;
	setAttr ".tk[24]" -type "float3" -1.1087432e-07 0 0.93008149 ;
	setAttr ".tk[25]" -type "float3" 0.28741115 0 0.88455981 ;
	setAttr ".tk[26]" -type "float3" 0.54668838 0 0.75245184 ;
	setAttr ".tk[27]" -type "float3" 0.75245184 0 0.54668826 ;
	setAttr ".tk[28]" -type "float3" 0.88455969 0 0.287411 ;
	setAttr ".tk[29]" -type "float3" 0.93008131 0 -1.6631148e-07 ;
	setAttr ".tk[30]" -type "float3" 0.88455969 0 -0.28741136 ;
	setAttr ".tk[31]" -type "float3" 0.75245178 0 -0.54668856 ;
	setAttr ".tk[32]" -type "float3" 0.54668826 0 -0.75245202 ;
	setAttr ".tk[33]" -type "float3" 0.28741106 0 -0.88455987 ;
	setAttr ".tk[34]" -type "float3" -8.315574e-08 0 -0.93008149 ;
	setAttr ".tk[35]" -type "float3" -0.28741121 0 -0.88455981 ;
	setAttr ".tk[36]" -type "float3" -0.54668838 0 -0.75245196 ;
	setAttr ".tk[37]" -type "float3" -0.75245184 0 -0.5466885 ;
	setAttr ".tk[38]" -type "float3" -0.88455969 0 -0.2874113 ;
	setAttr ".tk[39]" -type "float3" -0.93008131 0 -1.6631148e-07 ;
	setAttr ".tk[42]" -type "float3" -0.082117453 0 -0.25273132 ;
	setAttr ".tk[43]" -type "float3" -2.3758776e-08 0 -0.26573771 ;
	setAttr ".tk[44]" -type "float3" 0.082117356 0 -0.25273138 ;
	setAttr ".tk[45]" -type "float3" 0.15619656 0 -0.21498632 ;
	setAttr ".tk[46]" -type "float3" 0.21498621 0 -0.15619674 ;
	setAttr ".tk[47]" -type "float3" 0.25273129 0 -0.082117483 ;
	setAttr ".tk[48]" -type "float3" 0.26573759 0 -4.7517553e-08 ;
	setAttr ".tk[49]" -type "float3" 0.25273129 0 0.082117356 ;
	setAttr ".tk[50]" -type "float3" 0.21498629 0 0.15619656 ;
	setAttr ".tk[51]" -type "float3" 0.15619661 0 0.21498628 ;
	setAttr ".tk[52]" -type "float3" 0.082117401 0 0.25273132 ;
	setAttr ".tk[53]" -type "float3" -3.1678375e-08 0 0.26573771 ;
	setAttr ".tk[54]" -type "float3" -0.08211749 0 0.25273132 ;
	setAttr ".tk[55]" -type "float3" -0.15619676 0 0.21498634 ;
	setAttr ".tk[56]" -type "float3" -0.21498638 0 0.15619667 ;
	setAttr ".tk[57]" -type "float3" -0.25273171 0 0.082117371 ;
	setAttr ".tk[58]" -type "float3" -0.26573759 0 -4.7517553e-08 ;
	setAttr ".tk[59]" -type "float3" -0.25273129 0 -0.082117461 ;
	setAttr ".tk[60]" -type "float3" -0.21498626 0 -0.15619673 ;
	setAttr ".tk[61]" -type "float3" -0.15619661 0 -0.21498632 ;
	setAttr ".tk[62]" -type "float3" 0.016130207 0 0.049643677 ;
	setAttr ".tk[63]" -type "float3" 4.6669042e-09 0 0.052198447 ;
	setAttr ".tk[64]" -type "float3" -0.016130203 0 0.049643684 ;
	setAttr ".tk[65]" -type "float3" -0.030681472 0 0.042229433 ;
	setAttr ".tk[66]" -type "float3" -0.042229425 0 0.030681483 ;
	setAttr ".tk[67]" -type "float3" -0.049643677 0 0.016130216 ;
	setAttr ".tk[68]" -type "float3" -0.052198436 0 9.3338084e-09 ;
	setAttr ".tk[69]" -type "float3" -0.049643677 0 -0.016130196 ;
	setAttr ".tk[70]" -type "float3" -0.042229425 0 -0.030681472 ;
	setAttr ".tk[71]" -type "float3" -0.030681478 0 -0.042229425 ;
	setAttr ".tk[72]" -type "float3" -0.016130203 0 -0.04964368 ;
	setAttr ".tk[73]" -type "float3" 6.2225385e-09 0 -0.052198447 ;
	setAttr ".tk[74]" -type "float3" 0.016130216 0 -0.049643684 ;
	setAttr ".tk[75]" -type "float3" 0.030681493 0 -0.042229436 ;
	setAttr ".tk[76]" -type "float3" 0.042229459 0 -0.030681478 ;
	setAttr ".tk[77]" -type "float3" 0.049643692 0 -0.016130203 ;
	setAttr ".tk[78]" -type "float3" 0.052198436 0 9.3338084e-09 ;
	setAttr ".tk[79]" -type "float3" 0.049643673 0 0.016130215 ;
	setAttr ".tk[80]" -type "float3" 0.042229425 0 0.030681482 ;
	setAttr ".tk[81]" -type "float3" 0.030681478 0 0.042229425 ;
	setAttr ".tk[82]" -type "float3" 0.060121678 0 0.18503545 ;
	setAttr ".tk[83]" -type "float3" 1.7394825e-08 0 0.19455792 ;
	setAttr ".tk[84]" -type "float3" -0.06012167 0 0.18503545 ;
	setAttr ".tk[85]" -type "float3" -0.11435816 0 0.15740065 ;
	setAttr ".tk[86]" -type "float3" -0.15740064 0 0.11435831 ;
	setAttr ".tk[87]" -type "float3" -0.18503545 0 0.0601217 ;
	setAttr ".tk[88]" -type "float3" -0.19455791 0 3.478965e-08 ;
	setAttr ".tk[89]" -type "float3" -0.18503545 0 -0.060121663 ;
	setAttr ".tk[90]" -type "float3" -0.15740064 0 -0.11435817 ;
	setAttr ".tk[91]" -type "float3" -0.11435817 0 -0.15740064 ;
	setAttr ".tk[92]" -type "float3" -0.06012167 0 -0.18503545 ;
	setAttr ".tk[93]" -type "float3" 2.319311e-08 0 -0.19455792 ;
	setAttr ".tk[94]" -type "float3" 0.0601217 0 -0.18503545 ;
	setAttr ".tk[95]" -type "float3" 0.11435833 0 -0.15740065 ;
	setAttr ".tk[96]" -type "float3" 0.1574007 0 -0.11435817 ;
	setAttr ".tk[97]" -type "float3" 0.18503568 0 -0.06012167 ;
	setAttr ".tk[98]" -type "float3" 0.19455791 0 3.478965e-08 ;
	setAttr ".tk[99]" -type "float3" 0.18503544 0 0.060121685 ;
	setAttr ".tk[100]" -type "float3" 0.15740064 0 0.11435831 ;
	setAttr ".tk[101]" -type "float3" 0.11435817 0 0.15740065 ;
	setAttr ".tk[102]" -type "float3" 0.079184674 0 0.24370523 ;
	setAttr ".tk[103]" -type "float3" 2.2910255e-08 0 0.25624698 ;
	setAttr ".tk[104]" -type "float3" -0.079184614 0 0.24370523 ;
	setAttr ".tk[105]" -type "float3" -0.15061814 0 0.20730819 ;
	setAttr ".tk[106]" -type "float3" -0.20730817 0 0.15061826 ;
	setAttr ".tk[107]" -type "float3" -0.24370523 0 0.079184718 ;
	setAttr ".tk[108]" -type "float3" -0.25624698 0 4.582051e-08 ;
	setAttr ".tk[109]" -type "float3" -0.24370523 0 -0.079184607 ;
	setAttr ".tk[110]" -type "float3" -0.20730817 0 -0.15061814 ;
	setAttr ".tk[111]" -type "float3" -0.15061814 0 -0.20730817 ;
	setAttr ".tk[112]" -type "float3" -0.079184614 0 -0.24370523 ;
	setAttr ".tk[113]" -type "float3" 3.0547021e-08 0 -0.25624698 ;
	setAttr ".tk[114]" -type "float3" 0.079184711 0 -0.24370523 ;
	setAttr ".tk[115]" -type "float3" 0.15061827 0 -0.20730823 ;
	setAttr ".tk[116]" -type "float3" 0.20730825 0 -0.15061814 ;
	setAttr ".tk[117]" -type "float3" 0.24370553 0 -0.079184614 ;
	setAttr ".tk[118]" -type "float3" 0.25624698 0 4.582051e-08 ;
	setAttr ".tk[119]" -type "float3" 0.24370521 0 0.079184681 ;
	setAttr ".tk[120]" -type "float3" 0.20730817 0 0.15061826 ;
	setAttr ".tk[121]" -type "float3" 0.15061814 0 0.20730819 ;
	setAttr ".tk[122]" -type "float3" 0.048390619 0 0.14893101 ;
	setAttr ".tk[123]" -type "float3" 1.4000716e-08 0 0.15659536 ;
	setAttr ".tk[124]" -type "float3" -0.048390612 0 0.14893103 ;
	setAttr ".tk[125]" -type "float3" -0.092044391 0 0.12668829 ;
	setAttr ".tk[126]" -type "float3" -0.12668827 0 0.092044488 ;
	setAttr ".tk[127]" -type "float3" -0.14893101 0 0.048390649 ;
	setAttr ".tk[128]" -type "float3" -0.15659535 0 3.7335241e-08 ;
	setAttr ".tk[129]" -type "float3" -0.14893101 0 -0.048390605 ;
	setAttr ".tk[130]" -type "float3" -0.12668827 0 -0.092044391 ;
	setAttr ".tk[131]" -type "float3" -0.092044398 0 -0.12668827 ;
	setAttr ".tk[132]" -type "float3" -0.048390616 0 -0.14893101 ;
	setAttr ".tk[133]" -type "float3" 1.866762e-08 0 -0.15659536 ;
	setAttr ".tk[134]" -type "float3" 0.048390646 0 -0.14893101 ;
	setAttr ".tk[135]" -type "float3" 0.092044495 0 -0.12668829 ;
	setAttr ".tk[136]" -type "float3" 0.12668833 0 -0.092044398 ;
	setAttr ".tk[137]" -type "float3" 0.14893112 0 -0.048390612 ;
	setAttr ".tk[138]" -type "float3" 0.15659535 0 3.7335241e-08 ;
	setAttr ".tk[139]" -type "float3" 0.14893101 0 0.048390646 ;
	setAttr ".tk[140]" -type "float3" 0.12668827 0 0.09204448 ;
	setAttr ".tk[141]" -type "float3" 0.092044398 0 0.12668829 ;
	setAttr ".tk[142]" -type "float3" 0.010264678 0 0.03159143 ;
	setAttr ".tk[143]" -type "float3" 2.9698479e-09 0 0.033217199 ;
	setAttr ".tk[144]" -type "float3" -0.010264674 0 0.031591438 ;
	setAttr ".tk[145]" -type "float3" -0.019524571 0 0.026873279 ;
	setAttr ".tk[146]" -type "float3" -0.026873276 0 0.019524585 ;
	setAttr ".tk[147]" -type "float3" -0.031591423 0 0.010264686 ;
	setAttr ".tk[148]" -type "float3" -0.033217192 0 7.9195956e-09 ;
	setAttr ".tk[149]" -type "float3" -0.031591423 0 -0.010264669 ;
	setAttr ".tk[150]" -type "float3" -0.026873276 0 -0.019524571 ;
	setAttr ".tk[151]" -type "float3" -0.019524574 0 -0.026873276 ;
	setAttr ".tk[152]" -type "float3" -0.010264676 0 -0.031591423 ;
	setAttr ".tk[153]" -type "float3" 3.9597978e-09 0 -0.033217199 ;
	setAttr ".tk[154]" -type "float3" 0.010264684 0 -0.03159143 ;
	setAttr ".tk[155]" -type "float3" 0.019524587 0 -0.026873279 ;
	setAttr ".tk[156]" -type "float3" 0.026873291 0 -0.019524574 ;
	setAttr ".tk[157]" -type "float3" 0.031591445 0 -0.010264674 ;
	setAttr ".tk[158]" -type "float3" 0.033217192 0 7.9195956e-09 ;
	setAttr ".tk[159]" -type "float3" 0.031591423 0 0.010264684 ;
	setAttr ".tk[160]" -type "float3" 0.026873276 0 0.019524582 ;
	setAttr ".tk[161]" -type "float3" 0.019524574 0 0.026873279 ;
	setAttr ".tk[162]" -type "float3" -0.054256149 0 -0.16698325 ;
	setAttr ".tk[163]" -type "float3" -1.5697772e-08 0 -0.17557663 ;
	setAttr ".tk[164]" -type "float3" 0.054256141 0 -0.16698325 ;
	setAttr ".tk[165]" -type "float3" 0.10320129 0 -0.14204447 ;
	setAttr ".tk[166]" -type "float3" 0.14204445 0 -0.1032014 ;
	setAttr ".tk[167]" -type "float3" 0.16698323 0 -0.054256178 ;
	setAttr ".tk[168]" -type "float3" 0.17557663 0 -4.1860723e-08 ;
	setAttr ".tk[169]" -type "float3" 0.16698323 0 0.05425613 ;
	setAttr ".tk[170]" -type "float3" 0.14204445 0 0.10320129 ;
	setAttr ".tk[171]" -type "float3" 0.1032013 0 0.14204445 ;
	setAttr ".tk[172]" -type "float3" 0.054256145 0 0.16698323 ;
	setAttr ".tk[173]" -type "float3" -2.0930361e-08 0 0.17557663 ;
	setAttr ".tk[174]" -type "float3" -0.054256175 0 0.16698325 ;
	setAttr ".tk[175]" -type "float3" -0.10320142 0 0.14204447 ;
	setAttr ".tk[176]" -type "float3" -0.14204451 0 0.1032013 ;
	setAttr ".tk[177]" -type "float3" -0.16698338 0 0.054256141 ;
	setAttr ".tk[178]" -type "float3" -0.17557663 0 -4.1860723e-08 ;
	setAttr ".tk[179]" -type "float3" -0.16698323 0 -0.054256175 ;
	setAttr ".tk[180]" -type "float3" -0.14204445 0 -0.1032014 ;
	setAttr ".tk[181]" -type "float3" -0.1032013 0 -0.14204447 ;
	setAttr ".tk[182]" -type "float3" -0.086516619 0 -0.26627052 ;
	setAttr ".tk[183]" -type "float3" -2.5031563e-08 0 -0.27997357 ;
	setAttr ".tk[184]" -type "float3" 0.0865165 0 -0.26627058 ;
	setAttr ".tk[185]" -type "float3" 0.16456427 0 -0.22650349 ;
	setAttr ".tk[186]" -type "float3" 0.22650336 0 -0.16456445 ;
	setAttr ".tk[187]" -type "float3" 0.26627043 0 -0.086516649 ;
	setAttr ".tk[188]" -type "float3" 0.27997354 0 -6.6750864e-08 ;
	setAttr ".tk[189]" -type "float3" 0.26627043 0 0.08651647 ;
	setAttr ".tk[190]" -type "float3" 0.22650342 0 0.16456427 ;
	setAttr ".tk[191]" -type "float3" 0.16456431 0 0.22650336 ;
	setAttr ".tk[192]" -type "float3" 0.086516529 0 0.26627046 ;
	setAttr ".tk[193]" -type "float3" -3.3375432e-08 0 0.27997357 ;
	setAttr ".tk[194]" -type "float3" -0.086516641 0 0.26627046 ;
	setAttr ".tk[195]" -type "float3" -0.16456448 0 0.22650348 ;
	setAttr ".tk[196]" -type "float3" -0.22650354 0 0.16456431 ;
	setAttr ".tk[197]" -type "float3" -0.26627085 0 0.0865165 ;
	setAttr ".tk[198]" -type "float3" -0.27997354 0 -6.6750864e-08 ;
	setAttr ".tk[199]" -type "float3" -0.26627043 0 -0.086516641 ;
	setAttr ".tk[200]" -type "float3" -0.22650342 0 -0.16456443 ;
	setAttr ".tk[201]" -type "float3" -0.16456431 0 -0.22650348 ;
	setAttr ".tk[202]" -type "float3" -0.13783994 2.220446e-16 -0.42422745 ;
	setAttr ".tk[203]" -type "float3" -3.9880824e-08 2.220446e-16 -0.44605926 ;
	setAttr ".tk[204]" -type "float3" 0.13783982 2.220446e-16 -0.42422745 ;
	setAttr ".tk[205]" -type "float3" 0.26218727 2.220446e-16 -0.36087003 ;
	setAttr ".tk[206]" -type "float3" 0.36086988 2.220446e-16 -0.26218745 ;
	setAttr ".tk[207]" -type "float3" 0.42422736 2.220446e-16 -0.13784009 ;
	setAttr ".tk[208]" -type "float3" 0.44605923 2.220446e-16 -1.0634878e-07 ;
	setAttr ".tk[209]" -type "float3" 0.42422736 2.220446e-16 0.13783975 ;
	setAttr ".tk[210]" -type "float3" 0.36086988 2.220446e-16 0.26218727 ;
	setAttr ".tk[211]" -type "float3" 0.2621873 2.220446e-16 0.36086988 ;
	setAttr ".tk[212]" -type "float3" 0.13783988 2.220446e-16 0.42422739 ;
	setAttr ".tk[213]" -type "float3" -5.3174389e-08 2.220446e-16 0.44605926 ;
	setAttr ".tk[214]" -type "float3" -0.13784008 2.220446e-16 0.42422739 ;
	setAttr ".tk[215]" -type "float3" -0.26218745 2.220446e-16 0.36087003 ;
	setAttr ".tk[216]" -type "float3" -0.36087009 2.220446e-16 0.2621873 ;
	setAttr ".tk[217]" -type "float3" -0.42422777 2.220446e-16 0.13783982 ;
	setAttr ".tk[218]" -type "float3" -0.44605923 2.220446e-16 -1.0634878e-07 ;
	setAttr ".tk[219]" -type "float3" -0.42422736 2.220446e-16 -0.13784008 ;
	setAttr ".tk[220]" -type "float3" -0.36086988 2.220446e-16 -0.26218742 ;
	setAttr ".tk[221]" -type "float3" -0.2621873 2.220446e-16 -0.36087003 ;
	setAttr ".tk[222]" -type "float3" -0.16570126 0 -0.50997579 ;
	setAttr ".tk[223]" -type "float3" -4.7941814e-08 0 -0.53622055 ;
	setAttr ".tk[224]" -type "float3" 0.16570114 0 -0.50997579 ;
	setAttr ".tk[225]" -type "float3" 0.31518236 0 -0.43381158 ;
	setAttr ".tk[226]" -type "float3" 0.4338114 0 -0.31518266 ;
	setAttr ".tk[227]" -type "float3" 0.50997555 0 -0.16570136 ;
	setAttr ".tk[228]" -type "float3" 0.53622049 0 -1.278449e-07 ;
	setAttr ".tk[229]" -type "float3" 0.50997555 0 0.16570109 ;
	setAttr ".tk[230]" -type "float3" 0.4338114 0 0.31518236 ;
	setAttr ".tk[231]" -type "float3" 0.31518248 0 0.4338114 ;
	setAttr ".tk[232]" -type "float3" 0.1657012 0 0.50997567 ;
	setAttr ".tk[233]" -type "float3" -6.392245e-08 0 0.53622055 ;
	setAttr ".tk[234]" -type "float3" -0.16570134 0 0.50997567 ;
	setAttr ".tk[235]" -type "float3" -0.31518275 0 0.43381158 ;
	setAttr ".tk[236]" -type "float3" -0.43381175 0 0.31518248 ;
	setAttr ".tk[237]" -type "float3" -0.50997615 0 0.16570114 ;
	setAttr ".tk[238]" -type "float3" -0.53622049 0 -1.278449e-07 ;
	setAttr ".tk[239]" -type "float3" -0.50997555 0 -0.16570134 ;
	setAttr ".tk[240]" -type "float3" -0.4338114 0 -0.3151826 ;
	setAttr ".tk[241]" -type "float3" -0.31518248 0 -0.43381158 ;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "6EC99F74-4D62-209C-F56C-F29D3E73CDE9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.4306018054485321;
	setAttr ".dr" no;
	setAttr ".re" 55;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "E5F3D594-4BAF-533E-EB75-3183A5682DF6";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[242:261]" -type "float3"  -0.039049111 0 0.053746533
		 -0.053746525 0 0.039049122 -0.063182935 0 0.020529334 -0.066434376 0 1.1921612e-08
		 -0.063182935 0 -0.020529319 -0.053746525 0 -0.039049122 -0.039049111 0 -0.053746533
		 -0.020529324 0 -0.063182935 7.9195948e-09 0 -0.066434383 0.020529334 0 -0.063182935
		 0.039049126 0 -0.053746533 0.053746544 0 -0.039049122 0.063182935 0 -0.020529326
		 0.066434376 0 1.1921612e-08 0.063182935 0 0.020529328 0.053746525 0 0.039049111 0.039049122
		 0 0.053746533 0.020529324 0 0.063182928 5.9396981e-09 0 0.066434376 -0.020529324
		 0 0.063182928;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "4D9EDBE9-439F-3FD4-1F5F-1B909F84A86C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[100:101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]" "e[113]" "e[115]" "e[117]" "e[119]" "e[121]" "e[123]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.51293385028839111;
	setAttr ".re" 137;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "9B42AC6F-4651-A5C7-D2BE-089D85401493";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[262:281]" -type "float3"  0.035193171 0 0.10831336 1.4404691e-08
		 0 0.1138875 -0.035193156 0 0.10831337 -0.066941299 0 0.092136912 -0.092136905 0 0.066941313
		 -0.10831334 0 0.035193175 -0.1138875 0 2.0364638e-08 -0.10831334 0 -0.035193145 -0.092136905
		 0 -0.066941299 -0.066941313 0 -0.092136905 -0.035193164 0 -0.10831336 1.7798822e-08
		 0 -0.1138875 0.035193179 0 -0.10831337 0.066941336 0 -0.092136912 0.092136934 0 -0.066941313
		 0.10831337 0 -0.035193168 0.1138875 0 2.0364638e-08 0.10831336 0 0.035193171 0.092136912
		 0 0.066941313 0.066941313 0 0.092136912;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "D5CCA6E3-440F-2FF1-C68D-07BD5586A4AC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[140:141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[175]" "e[177]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.52262705564498901;
	setAttr ".dr" no;
	setAttr ".re" 175;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "EC3EEEA9-4133-0D94-9678-54A154135E1F";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[282:301]" -type "float3"  0.022313802 0 0.030712312
		 0.01173106 0 0.036104497 3.3941119e-09 0 0.037962507 -0.011731057 0 0.0361045 -0.022313796
		 0 0.030712316 -0.03071231 0 0.022313805 -0.036104493 0 0.011731067 -0.0379625 0 6.7882246e-09
		 -0.036104493 0 -0.011731056 -0.03071231 0 -0.0223138 -0.022313802 0 -0.03071231 -0.011731058
		 0 -0.036104497 4.5254827e-09 0 -0.037962507 0.011731067 0 -0.0361045 0.022313811
		 0 -0.03071232 0.030712333 0 -0.022313802 0.036104504 0 -0.011731058 0.0379625 0 6.7882246e-09
		 0.036104493 0 0.011731065 0.03071231 0 0.022313803;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "E1C06303-4D20-EA2B-6B55-34B4B55A24B4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[180:181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[211]" "e[213]" "e[215]" "e[217]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.48358583450317383;
	setAttr ".dr" no;
	setAttr ".re" 217;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "29BCE5A3-4D38-E479-2862-8EBF8845B07C";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[302:321]" -type "float3"  0.015356153 0 0.011156903
		 0.0111569 0 0.015356157 0.005865531 0 0.018052243 1.6970559e-09 0 0.018981256 -0.0058655278
		 0 0.018052246 -0.011156898 0 0.015356158 -0.015356155 0 0.011156904 -0.018052243
		 0 0.0058655329 -0.018981252 0 3.3941119e-09 -0.018052243 0 -0.0058655269 -0.015356157
		 0 -0.011156899 -0.0111569 0 -0.015356156 -0.0058655287 0 -0.018052245 2.2627413e-09
		 0 -0.018981256 0.0058655329 0 -0.018052245 0.011156905 0 -0.015356161 0.015356166
		 0 -0.0111569 0.018052254 0 -0.0058655287 0.018981252 0 3.3941119e-09 0.018052243
		 0 0.0058655315;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "EDB48DDE-4EC8-D3EC-773B-DBB0413D90F7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[220:221]" "e[223]" "e[225]" "e[227]" "e[229]" "e[231]" "e[233]" "e[235]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.49426233768463135;
	setAttr ".re" 225;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "E0247C17-4660-DDE6-98B6-48BFEE55CB34";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[322:341]" -type "float3"  0.011156899 0 0.015356158
		 0.0058655301 0 0.018052243 1.6970558e-09 0 0.018981256 -0.0058655273 0 0.018052243
		 -0.011156897 0 0.015356158 -0.015356155 0 0.011156904 -0.018052243 0 0.0058655334
		 -0.018981252 0 3.3941117e-09 -0.018052243 0 -0.0058655264 -0.015356155 0 -0.011156898
		 -0.011156899 0 -0.015356155 -0.0058655278 0 -0.018052243 2.2627413e-09 0 -0.018981256
		 0.0058655329 0 -0.018052246 0.011156905 0 -0.015356159 0.015356165 0 -0.011156899
		 0.018052252 0 -0.0058655278 0.018981252 0 3.3941117e-09 0.018052241 0 0.0058655315
		 0.015356155 0 0.011156903;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "56618459-46E0-DE93-699B-78B8F10D248F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[260:261]" "e[263]" "e[265]" "e[267]" "e[269]" "e[271]" "e[273]" "e[275]" "e[277]" "e[279]" "e[281]" "e[283]" "e[285]" "e[287]" "e[289]" "e[291]" "e[293]" "e[295]" "e[297]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.50880593061447144;
	setAttr ".dr" no;
	setAttr ".re" 263;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "2E57BC3E-42DF-6D60-4DE5-69B70D11C75F";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[342:361]" -type "float3"  -0.0055784485 0 0.0076780799
		 -0.0076780771 0 0.0055784518 -0.0090261213 0 0.0029327669 -0.0094906259 0 2.2020041e-09
		 -0.0090261213 0 -0.0029327627 -0.007678078 0 -0.0055784485 -0.0055784495 0 -0.0076780771
		 -0.0029327641 0 -0.0090261223 1.1313707e-09 0 -0.0094906278 0.0029327665 0 -0.0090261223
		 0.0055784527 0 -0.0076780799 0.0076780827 0 -0.005578449 0.0090261269 0 -0.0029327637
		 0.0094906259 0 2.2020041e-09 0.0090261213 0 0.0029327665 0.0076780766 0 0.0055784518
		 0.0055784495 0 0.0076780799 0.0029327651 0 0.0090261223 8.4852791e-10 0 0.0094906278
		 -0.0029327637 0 0.0090261241;
createNode polySplitRing -n "polySplitRing18";
	rename -uid "367CD694-4286-1FD1-4717-CC89D5AC425E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[500:501]" "e[503]" "e[505]" "e[507]" "e[509]" "e[511]" "e[513]" "e[515]" "e[517]" "e[519]" "e[521]" "e[523]" "e[525]" "e[527]" "e[529]" "e[531]" "e[533]" "e[535]" "e[537]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.57953673601150513;
	setAttr ".re" 537;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "E10A8322-4AD0-5407-A1DA-67A56CEB4BFF";
	setAttr ".uopa" yes;
	setAttr -s 180 ".tk";
	setAttr ".tk[20]" -type "float3" -0.02527323 -0.0063161273 0.0082116574 ;
	setAttr ".tk[21]" -type "float3" -0.021498535 -0.0063161273 0.015619562 ;
	setAttr ".tk[22]" -type "float3" -0.015619562 -0.0063161273 0.021498535 ;
	setAttr ".tk[23]" -type "float3" -0.0082116574 -0.0063161273 0.02527323 ;
	setAttr ".tk[24]" -type "float3" -3.1678378e-09 -0.0063161273 0.026573755 ;
	setAttr ".tk[25]" -type "float3" 0.0082116574 -0.0063161273 0.02527323 ;
	setAttr ".tk[26]" -type "float3" 0.015619562 -0.0063161273 0.021498535 ;
	setAttr ".tk[27]" -type "float3" 0.021498535 -0.0063161273 0.015619562 ;
	setAttr ".tk[28]" -type "float3" 0.02527323 -0.0063161273 0.0082116574 ;
	setAttr ".tk[29]" -type "float3" 0.026573755 -0.0063161273 -4.7517568e-09 ;
	setAttr ".tk[30]" -type "float3" 0.02527323 -0.0063161273 -0.0082116574 ;
	setAttr ".tk[31]" -type "float3" 0.021498535 -0.0063161273 -0.015619562 ;
	setAttr ".tk[32]" -type "float3" 0.015619562 -0.0063161273 -0.021498535 ;
	setAttr ".tk[33]" -type "float3" 0.0082116574 -0.0063161273 -0.02527323 ;
	setAttr ".tk[34]" -type "float3" -2.3758784e-09 -0.0063161273 -0.026573755 ;
	setAttr ".tk[35]" -type "float3" -0.0082116574 -0.0063161273 -0.02527323 ;
	setAttr ".tk[36]" -type "float3" -0.015619562 -0.0063161273 -0.021498535 ;
	setAttr ".tk[37]" -type "float3" -0.021498535 -0.0063161273 -0.015619562 ;
	setAttr ".tk[38]" -type "float3" -0.02527323 -0.0063161273 -0.0082116574 ;
	setAttr ".tk[39]" -type "float3" -0.026573755 -0.0063161273 -4.7517568e-09 ;
	setAttr ".tk[122]" -type "float3" -0.0087982956 0 -0.027078366 ;
	setAttr ".tk[123]" -type "float3" -2.545584e-09 0 -0.028471882 ;
	setAttr ".tk[124]" -type "float3" 0.0087982919 0 -0.027078375 ;
	setAttr ".tk[125]" -type "float3" 0.016735347 0 -0.023034241 ;
	setAttr ".tk[126]" -type "float3" 0.02303423 0 -0.016735358 ;
	setAttr ".tk[127]" -type "float3" 0.027078364 0 -0.0087983012 ;
	setAttr ".tk[128]" -type "float3" 0.028471876 0 -6.7882238e-09 ;
	setAttr ".tk[129]" -type "float3" 0.027078364 0 0.0087982882 ;
	setAttr ".tk[130]" -type "float3" 0.023034234 0 0.016735347 ;
	setAttr ".tk[131]" -type "float3" 0.016735351 0 0.02303423 ;
	setAttr ".tk[132]" -type "float3" 0.0087982938 0 0.027078366 ;
	setAttr ".tk[133]" -type "float3" -3.3941119e-09 0 0.028471882 ;
	setAttr ".tk[134]" -type "float3" -0.0087983003 0 0.027078366 ;
	setAttr ".tk[135]" -type "float3" -0.01673536 0 0.023034239 ;
	setAttr ".tk[136]" -type "float3" -0.02303425 0 0.016735349 ;
	setAttr ".tk[137]" -type "float3" -0.027078383 0 0.0087982919 ;
	setAttr ".tk[138]" -type "float3" -0.028471876 0 -6.7882238e-09 ;
	setAttr ".tk[139]" -type "float3" -0.027078364 0 -0.0087983003 ;
	setAttr ".tk[140]" -type "float3" -0.02303423 0 -0.016735356 ;
	setAttr ".tk[141]" -type "float3" -0.016735351 0 -0.023034239 ;
	setAttr ".tk[142]" -type "float3" -0.0087982947 0 -0.027078371 ;
	setAttr ".tk[143]" -type "float3" -2.545584e-09 0 -0.028471882 ;
	setAttr ".tk[144]" -type "float3" 0.008798291 0 -0.027078375 ;
	setAttr ".tk[145]" -type "float3" 0.016735349 0 -0.023034241 ;
	setAttr ".tk[146]" -type "float3" 0.023034232 0 -0.016735358 ;
	setAttr ".tk[147]" -type "float3" 0.027078364 0 -0.0087983022 ;
	setAttr ".tk[148]" -type "float3" 0.028471876 0 -6.7882238e-09 ;
	setAttr ".tk[149]" -type "float3" 0.027078364 0 0.0087982882 ;
	setAttr ".tk[150]" -type "float3" 0.023034234 0 0.016735349 ;
	setAttr ".tk[151]" -type "float3" 0.016735351 0 0.023034232 ;
	setAttr ".tk[152]" -type "float3" 0.0087982928 0 0.027078366 ;
	setAttr ".tk[153]" -type "float3" -3.3941119e-09 0 0.028471882 ;
	setAttr ".tk[154]" -type "float3" -0.0087983003 0 0.027078371 ;
	setAttr ".tk[155]" -type "float3" -0.01673536 0 0.023034239 ;
	setAttr ".tk[156]" -type "float3" -0.023034252 0 0.016735349 ;
	setAttr ".tk[157]" -type "float3" -0.027078379 0 0.008798291 ;
	setAttr ".tk[158]" -type "float3" -0.028471876 0 -6.7882238e-09 ;
	setAttr ".tk[159]" -type "float3" -0.027078364 0 -0.0087983003 ;
	setAttr ".tk[160]" -type "float3" -0.023034232 0 -0.016735356 ;
	setAttr ".tk[161]" -type "float3" -0.016735351 0 -0.023034239 ;
	setAttr ".tk[162]" -type "float3" -0.014663829 0 -0.045130622 ;
	setAttr ".tk[163]" -type "float3" -4.24264e-09 0 -0.047453135 ;
	setAttr ".tk[164]" -type "float3" 0.01466382 0 -0.045130625 ;
	setAttr ".tk[165]" -type "float3" 0.027892252 0 -0.038390405 ;
	setAttr ".tk[166]" -type "float3" 0.038390387 0 -0.027892262 ;
	setAttr ".tk[167]" -type "float3" 0.045130614 0 -0.01466384 ;
	setAttr ".tk[168]" -type "float3" 0.047453124 0 -1.1313706e-08 ;
	setAttr ".tk[169]" -type "float3" 0.045130614 0 0.014663813 ;
	setAttr ".tk[170]" -type "float3" 0.038390387 0 0.027892252 ;
	setAttr ".tk[171]" -type "float3" 0.027892254 0 0.038390387 ;
	setAttr ".tk[172]" -type "float3" 0.014663823 0 0.045130622 ;
	setAttr ".tk[173]" -type "float3" -5.6568532e-09 0 0.047453135 ;
	setAttr ".tk[174]" -type "float3" -0.014663834 0 0.045130622 ;
	setAttr ".tk[175]" -type "float3" -0.027892271 0 0.038390398 ;
	setAttr ".tk[176]" -type "float3" -0.038390417 0 0.027892254 ;
	setAttr ".tk[177]" -type "float3" -0.045130633 0 0.01466382 ;
	setAttr ".tk[178]" -type "float3" -0.047453124 0 -1.1313706e-08 ;
	setAttr ".tk[179]" -type "float3" -0.04513061 0 -0.014663834 ;
	setAttr ".tk[180]" -type "float3" -0.038390387 0 -0.027892256 ;
	setAttr ".tk[181]" -type "float3" -0.027892254 0 -0.038390398 ;
	setAttr ".tk[182]" -type "float3" -0.010264678 0 -0.031591441 ;
	setAttr ".tk[183]" -type "float3" -2.9698488e-09 0 -0.033217199 ;
	setAttr ".tk[184]" -type "float3" 0.010264676 0 -0.031591441 ;
	setAttr ".tk[185]" -type "float3" 0.019524576 0 -0.026873281 ;
	setAttr ".tk[186]" -type "float3" 0.026873268 0 -0.019524578 ;
	setAttr ".tk[187]" -type "float3" 0.031591434 0 -0.010264685 ;
	setAttr ".tk[188]" -type "float3" 0.033217188 0 -7.9195939e-09 ;
	setAttr ".tk[189]" -type "float3" 0.031591434 0 0.010264671 ;
	setAttr ".tk[190]" -type "float3" 0.026873268 0 0.019524576 ;
	setAttr ".tk[191]" -type "float3" 0.019524576 0 0.026873268 ;
	setAttr ".tk[192]" -type "float3" 0.010264678 0 0.031591438 ;
	setAttr ".tk[193]" -type "float3" -3.9597969e-09 0 0.033217199 ;
	setAttr ".tk[194]" -type "float3" -0.010264684 0 0.031591441 ;
	setAttr ".tk[195]" -type "float3" -0.019524589 0 0.026873268 ;
	setAttr ".tk[196]" -type "float3" -0.026873291 0 0.019524576 ;
	setAttr ".tk[197]" -type "float3" -0.031591441 0 0.010264676 ;
	setAttr ".tk[198]" -type "float3" -0.033217188 0 -7.9195939e-09 ;
	setAttr ".tk[199]" -type "float3" -0.031591434 0 -0.010264684 ;
	setAttr ".tk[200]" -type "float3" -0.026873268 0 -0.01952458 ;
	setAttr ".tk[201]" -type "float3" -0.019524576 0 -0.026873268 ;
	setAttr ".tk[202]" -type "float3" -0.010264674 1.110223e-16 -0.031591438 ;
	setAttr ".tk[203]" -type "float3" -2.9698461e-09 1.110223e-16 -0.033217199 ;
	setAttr ".tk[204]" -type "float3" 0.010264674 1.110223e-16 -0.031591438 ;
	setAttr ".tk[205]" -type "float3" 0.019524554 1.110223e-16 -0.026873251 ;
	setAttr ".tk[206]" -type "float3" 0.026873244 1.110223e-16 -0.019524565 ;
	setAttr ".tk[207]" -type "float3" 0.031591434 1.110223e-16 -0.01026468 ;
	setAttr ".tk[208]" -type "float3" 0.033217188 1.110223e-16 -7.9195948e-09 ;
	setAttr ".tk[209]" -type "float3" 0.031591434 1.110223e-16 0.01026467 ;
	setAttr ".tk[210]" -type "float3" 0.026873244 1.110223e-16 0.019524554 ;
	setAttr ".tk[211]" -type "float3" 0.019524561 1.110223e-16 0.026873244 ;
	setAttr ".tk[212]" -type "float3" 0.010264674 1.110223e-16 0.031591438 ;
	setAttr ".tk[213]" -type "float3" -3.9597974e-09 1.110223e-16 0.033217199 ;
	setAttr ".tk[214]" -type "float3" -0.010264676 1.110223e-16 0.031591438 ;
	setAttr ".tk[215]" -type "float3" -0.019524574 1.110223e-16 0.026873251 ;
	setAttr ".tk[216]" -type "float3" -0.026873268 1.110223e-16 0.019524561 ;
	setAttr ".tk[217]" -type "float3" -0.031591441 1.110223e-16 0.010264674 ;
	setAttr ".tk[218]" -type "float3" -0.033217188 1.110223e-16 -7.9195948e-09 ;
	setAttr ".tk[219]" -type "float3" -0.031591434 1.110223e-16 -0.010264676 ;
	setAttr ".tk[220]" -type "float3" -0.026873244 1.110223e-16 -0.019524561 ;
	setAttr ".tk[221]" -type "float3" -0.019524561 1.110223e-16 -0.026873251 ;
	setAttr ".tk[222]" -type "float3" -0.013197442 0 -0.040617585 ;
	setAttr ".tk[223]" -type "float3" -3.8183789e-09 0 -0.042707823 ;
	setAttr ".tk[224]" -type "float3" 0.013197442 0 -0.040617585 ;
	setAttr ".tk[225]" -type "float3" 0.025103029 0 -0.03455136 ;
	setAttr ".tk[226]" -type "float3" 0.034551356 0 -0.025103033 ;
	setAttr ".tk[227]" -type "float3" 0.040617585 0 -0.013197456 ;
	setAttr ".tk[228]" -type "float3" 0.042707812 0 -1.0182336e-08 ;
	setAttr ".tk[229]" -type "float3" 0.040617585 0 0.013197436 ;
	setAttr ".tk[230]" -type "float3" 0.034551356 0 0.025103029 ;
	setAttr ".tk[231]" -type "float3" 0.025103029 0 0.034551356 ;
	setAttr ".tk[232]" -type "float3" 0.013197442 0 0.040617585 ;
	setAttr ".tk[233]" -type "float3" -5.0911679e-09 0 0.042707823 ;
	setAttr ".tk[234]" -type "float3" -0.013197451 0 0.040617585 ;
	setAttr ".tk[235]" -type "float3" -0.02510304 0 0.03455136 ;
	setAttr ".tk[236]" -type "float3" -0.034551378 0 0.025103029 ;
	setAttr ".tk[237]" -type "float3" -0.040617585 0 0.013197442 ;
	setAttr ".tk[238]" -type "float3" -0.042707812 0 -1.0182336e-08 ;
	setAttr ".tk[239]" -type "float3" -0.040617585 0 -0.013197451 ;
	setAttr ".tk[240]" -type "float3" -0.034551356 0 -0.025103029 ;
	setAttr ".tk[241]" -type "float3" -0.025103029 0 -0.03455136 ;
	setAttr ".tk[242]" -type "float3" 0.022313781 0 -0.030712303 ;
	setAttr ".tk[243]" -type "float3" 0.030712297 0 -0.022313787 ;
	setAttr ".tk[244]" -type "float3" 0.036104534 0 -0.011731049 ;
	setAttr ".tk[245]" -type "float3" 0.037962504 0 -6.8123502e-09 ;
	setAttr ".tk[246]" -type "float3" 0.036104534 0 0.01173104 ;
	setAttr ".tk[247]" -type "float3" 0.030712297 0 0.022313787 ;
	setAttr ".tk[248]" -type "float3" 0.022313781 0 0.030712303 ;
	setAttr ".tk[249]" -type "float3" 0.011731043 0 0.036104534 ;
	setAttr ".tk[250]" -type "float3" -4.5254827e-09 0 0.037962504 ;
	setAttr ".tk[251]" -type "float3" -0.011731049 0 0.036104534 ;
	setAttr ".tk[252]" -type "float3" -0.022313789 0 0.030712305 ;
	setAttr ".tk[253]" -type "float3" -0.030712316 0 0.022313787 ;
	setAttr ".tk[254]" -type "float3" -0.036104534 0 0.011731043 ;
	setAttr ".tk[255]" -type "float3" -0.037962504 0 -6.8123502e-09 ;
	setAttr ".tk[256]" -type "float3" -0.036104534 0 -0.011731044 ;
	setAttr ".tk[257]" -type "float3" -0.030712297 0 -0.022313777 ;
	setAttr ".tk[258]" -type "float3" -0.022313785 0 -0.030712303 ;
	setAttr ".tk[259]" -type "float3" -0.011731043 0 -0.03610453 ;
	setAttr ".tk[260]" -type "float3" -3.394113e-09 0 -0.037962507 ;
	setAttr ".tk[261]" -type "float3" 0.011731043 0 -0.03610453 ;
	setAttr ".tk[362]" -type "float3" 0.010264672 0 -0.031591434 ;
	setAttr ".tk[363]" -type "float3" 0.019524571 0 -0.026873279 ;
	setAttr ".tk[364]" -type "float3" 0.026873268 0 -0.019524584 ;
	setAttr ".tk[365]" -type "float3" 0.031591423 0 -0.010264684 ;
	setAttr ".tk[366]" -type "float3" 0.033217188 0 -7.9195948e-09 ;
	setAttr ".tk[367]" -type "float3" 0.031591423 0 0.010264669 ;
	setAttr ".tk[368]" -type "float3" 0.02687327 0 0.019524571 ;
	setAttr ".tk[369]" -type "float3" 0.019524574 0 0.026873268 ;
	setAttr ".tk[370]" -type "float3" 0.010264675 0 0.031591427 ;
	setAttr ".tk[371]" -type "float3" -3.9597974e-09 0 0.033217195 ;
	setAttr ".tk[372]" -type "float3" -0.010264683 0 0.031591427 ;
	setAttr ".tk[373]" -type "float3" -0.019524585 0 0.026873279 ;
	setAttr ".tk[374]" -type "float3" -0.026873291 0 0.019524574 ;
	setAttr ".tk[375]" -type "float3" -0.031591441 0 0.010264674 ;
	setAttr ".tk[376]" -type "float3" -0.033217188 0 -7.9195948e-09 ;
	setAttr ".tk[377]" -type "float3" -0.031591423 0 -0.010264683 ;
	setAttr ".tk[378]" -type "float3" -0.026873268 0 -0.019524582 ;
	setAttr ".tk[379]" -type "float3" -0.019524574 0 -0.026873279 ;
	setAttr ".tk[380]" -type "float3" -0.010264677 0 -0.031591427 ;
	setAttr ".tk[381]" -type "float3" -2.9698479e-09 0 -0.033217195 ;
createNode polySplitRing -n "polySplitRing19";
	rename -uid "2F770DCF-49EF-565F-DDC3-B6808FDA19B3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[300:301]" "e[303]" "e[305]" "e[307]" "e[309]" "e[311]" "e[313]" "e[315]" "e[317]" "e[319]" "e[321]" "e[323]" "e[325]" "e[327]" "e[329]" "e[331]" "e[333]" "e[335]" "e[337]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.52082955837249756;
	setAttr ".dr" no;
	setAttr ".re" 307;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak9";
	rename -uid "806D275B-4AB9-15DE-1485-88BDA8009B2A";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[382:401]" -type "float3"  -0.0093848193 0 0.028883636
		 -0.01785101 0 0.024569824 -0.024569824 0 0.01785101 -0.02888364 0 0.0093848221 -0.030370001
		 0 5.6984231e-09 -0.02888364 0 -0.0093848174 -0.024569824 0 -0.01785101 -0.01785101
		 0 -0.024569824 -0.0093848193 0 -0.028883636 2.0366109e-09 0 -0.030370001 0.0093848212
		 0 -0.028883636 0.01785101 0 -0.024569826 0.024569832 0 -0.01785101 0.028883636 0
		 -0.0093848193 0.030370001 0 5.6984231e-09 0.028883636 0 0.0093848212 0.024569821
		 0 0.017851008 0.017851008 0 0.024569824 0.0093848174 0 0.028883636 1.1315163e-09
		 0 0.030370001;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "49ABB986-44BF-419F-3ADF-1FAE6BA0BB29";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 710\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 710\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 710\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n"
		+ "            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n"
		+ "            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n"
		+ "            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n"
		+ "                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n"
		+ "                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 50 100 -ps 2 50 100 $gMainPane;\"\n"
		+ "\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1FD8D958-4B38-39E2-AACF-3F8F54AF482D";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCylinder -n "polyCylinder2";
	rename -uid "98D9E2FF-4168-DEE3-7A7C-35A0455317C1";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "6516CF30-49DD-3B98-2DCA-ABBFC5AFFC2B";
	setAttr ".ics" -type "componentList" 3 "f[156:157]" "f[373:374]" "f[394:395]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.0146698 0.21964951 0.3575156 ;
	setAttr ".rs" 37719;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.81285601854324341 -0.16468072889805188 1.1313718850658461e-09 ;
	setAttr ".cbx" -type "double3" 1.2164835929870605 0.6039797605524535 0.7150312066078186 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "A77477F8-434D-4CA9-3635-8D87359C535A";
	setAttr ".uopa" yes;
	setAttr -s 60 ".tk";
	setAttr ".tk[122]" -type "float3" 0.0035193181 0 0.010831347 ;
	setAttr ".tk[123]" -type "float3" 1.0182336e-09 0 0.011388753 ;
	setAttr ".tk[124]" -type "float3" -0.0035193167 0 0.010831349 ;
	setAttr ".tk[125]" -type "float3" -0.0066941385 0 0.0092136953 ;
	setAttr ".tk[126]" -type "float3" -0.0092136925 0 0.0066941427 ;
	setAttr ".tk[127]" -type "float3" -0.010831345 0 0.0035193204 ;
	setAttr ".tk[128]" -type "float3" -0.011388751 0 2.7152895e-09 ;
	setAttr ".tk[129]" -type "float3" -0.010831345 0 -0.0035193153 ;
	setAttr ".tk[130]" -type "float3" -0.0092136934 0 -0.0066941385 ;
	setAttr ".tk[131]" -type "float3" -0.0066941399 0 -0.0092136925 ;
	setAttr ".tk[132]" -type "float3" -0.0035193174 0 -0.010831347 ;
	setAttr ".tk[133]" -type "float3" 1.3576448e-09 0 -0.011388753 ;
	setAttr ".tk[134]" -type "float3" 0.0035193199 0 -0.010831347 ;
	setAttr ".tk[135]" -type "float3" 0.0066941427 0 -0.0092136953 ;
	setAttr ".tk[136]" -type "float3" 0.009213699 0 -0.006694139 ;
	setAttr ".tk[137]" -type "float3" 0.010831352 0 -0.0035193167 ;
	setAttr ".tk[138]" -type "float3" 0.011388751 0 2.7152895e-09 ;
	setAttr ".tk[139]" -type "float3" 0.010831345 0 0.0035193199 ;
	setAttr ".tk[140]" -type "float3" 0.0092136925 0 0.0066941418 ;
	setAttr ".tk[141]" -type "float3" 0.0066941399 0 0.0092136953 ;
	setAttr ".tk[362]" -type "float3" -0.0046924222 0 0.014441799 ;
	setAttr ".tk[363]" -type "float3" -0.008925518 0 0.012284927 ;
	setAttr ".tk[364]" -type "float3" -0.012284923 0 0.0089255236 ;
	setAttr ".tk[365]" -type "float3" -0.014441794 0 0.0046924273 ;
	setAttr ".tk[366]" -type "float3" -0.015185001 0 3.620386e-09 ;
	setAttr ".tk[367]" -type "float3" -0.014441794 0 -0.0046924204 ;
	setAttr ".tk[368]" -type "float3" -0.012284924 0 -0.008925518 ;
	setAttr ".tk[369]" -type "float3" -0.0089255199 0 -0.012284923 ;
	setAttr ".tk[370]" -type "float3" -0.0046924232 0 -0.014441796 ;
	setAttr ".tk[371]" -type "float3" 1.810193e-09 0 -0.015185004 ;
	setAttr ".tk[372]" -type "float3" 0.0046924264 0 -0.014441796 ;
	setAttr ".tk[373]" -type "float3" 0.0089255245 0 -0.012284927 ;
	setAttr ".tk[374]" -type "float3" 0.012284932 0 -0.0089255199 ;
	setAttr ".tk[375]" -type "float3" 0.014441803 0 -0.0046924227 ;
	setAttr ".tk[376]" -type "float3" 0.015185001 0 3.620386e-09 ;
	setAttr ".tk[377]" -type "float3" 0.014441794 0 0.0046924264 ;
	setAttr ".tk[378]" -type "float3" 0.012284923 0 0.0089255227 ;
	setAttr ".tk[379]" -type "float3" 0.0089255199 0 0.012284927 ;
	setAttr ".tk[380]" -type "float3" 0.0046924241 0 0.014441796 ;
	setAttr ".tk[381]" -type "float3" 1.3576448e-09 0 0.015185004 ;
	setAttr ".tk[402]" -type "float3" -0.0092136925 0 0.0066941427 ;
	setAttr ".tk[403]" -type "float3" -0.010831346 0 0.0035193213 ;
	setAttr ".tk[404]" -type "float3" -0.011388751 0 3.0983229e-09 ;
	setAttr ".tk[405]" -type "float3" -0.010831346 0 -0.0035193146 ;
	setAttr ".tk[406]" -type "float3" -0.0092136934 0 -0.006694139 ;
	setAttr ".tk[407]" -type "float3" -0.0066941399 0 -0.0092136925 ;
	setAttr ".tk[408]" -type "float3" -0.0035193174 0 -0.010831347 ;
	setAttr ".tk[409]" -type "float3" 1.3576448e-09 0 -0.011388753 ;
	setAttr ".tk[410]" -type "float3" 0.0035193204 0 -0.010831347 ;
	setAttr ".tk[411]" -type "float3" 0.0066941427 0 -0.0092136953 ;
	setAttr ".tk[412]" -type "float3" 0.009213699 0 -0.0066941399 ;
	setAttr ".tk[413]" -type "float3" 0.010831352 0 -0.0035193162 ;
	setAttr ".tk[414]" -type "float3" 0.011388751 0 3.0983229e-09 ;
	setAttr ".tk[415]" -type "float3" 0.010831346 0 0.0035193209 ;
	setAttr ".tk[416]" -type "float3" 0.0092136925 0 0.0066941418 ;
	setAttr ".tk[417]" -type "float3" 0.0066941399 0 0.0092136953 ;
	setAttr ".tk[418]" -type "float3" 0.0035193181 0 0.010831349 ;
	setAttr ".tk[419]" -type "float3" 1.0182335e-09 0 0.011388753 ;
	setAttr ".tk[420]" -type "float3" -0.0035193167 0 0.01083135 ;
	setAttr ".tk[421]" -type "float3" -0.0066941385 0 0.0092136972 ;
createNode polySplitRing -n "polySplitRing20";
	rename -uid "E0E233F2-4323-53B4-66ED-099ADCB879FE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[853]" "e[855]" "e[859]" "e[861]" "e[864:865]" "e[869]" "e[872]" "e[874]" "e[877]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.2841021716594696;
	setAttr ".re" 877;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "B93D6DC5-4EE0-91A1-4DBF-F09902A25F7A";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[420:431]" -type "float3"  0.79914576 -0.44982439 0.24972719
		 0.83307481 -0.44217429 -0.018435866 0.89685136 -0.5565331 0.25379354 0.92888319 -0.54931146
		 0.0006268362 0.95839101 -0.42266199 -0.26356307 1.047190666 -0.53089005 -0.23079215
		 0.67299199 -0.29967108 0.24371395 0.70921284 -0.29150453 -0.042562861 0.84299362
		 -0.27067435 -0.30424777 0.9992609 -0.65847158 0.25744462 1.029177427 -0.6517269 0.020996464
		 1.13967204 -0.63452238 -0.19514024;
createNode polySplitRing -n "polySplitRing21";
	rename -uid "5F91BDF7-46B7-40B1-909B-5386AA1BA5E4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[880:881]" "e[883]" "e[885]" "e[887]" "e[889]" "e[891]" "e[893]" "e[895]" "e[897]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1.6999999752404025 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.64060622453689575;
	setAttr ".dr" no;
	setAttr ".re" 893;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "F856D992-459F-4261-DBED-9D9E275E9E15";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[432:441]" -type "float3"  0 -0.087892801 0 0 -0.087892801
		 0 0 -0.087892801 0 0 -0.087892801 0 0 -0.087892801 0 0 -0.087892801 0 0 -0.087892801
		 0 0 -0.087892801 0 0 -0.087892801 0 0 -0.087892801 0;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "AB919AA6-4AFA-C29F-3AE5-7180BD02A436";
	setAttr ".ics" -type "componentList" 1 "f[40:59]";
	setAttr ".ix" -type "matrix" 0.33621478516182851 -0.049091687299176547 0 0 0.027634492084624949 0.18926065349243507 -0.090080864129987503 0
		 0.020916900044944616 0.14325380612820085 0.30739421895277774 0 0.14023852756181071 1.6765711932058311 -0.2933694425771734 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.16787298 1.8658319 -0.38345036 ;
	setAttr ".rs" 51117;
	setAttr ".lt" -type "double3" -4.8572257327350599e-17 1.1102230246251565e-16 0.3420185114453671 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.11310601738469805 1.7392943249547781 -0.64034369714117334 ;
	setAttr ".cbx" -type "double3" 0.44885197523793691 1.9923693157471547 -0.12655702620588788 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "7526E391-475F-DE40-39A4-968FE94475BC";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[0:39]" -type "float3"  0.088534527 2.9927716e-09
		 -0.028766586 0.075311966 2.9927716e-09 -0.054717325 0.05471734 2.9927716e-09 -0.075311944
		 0.02876661 2.9927716e-09 -0.088534474 9.9694866e-09 2.9927716e-09 -0.093090653 -0.028766597
		 2.9927716e-09 -0.088534474 -0.054717317 2.9927716e-09 -0.075311922 -0.075311929 2.9927716e-09
		 -0.054717299 -0.088534467 2.9927716e-09 -0.028766578 -0.093090639 2.9927716e-09 1.7347544e-08
		 -0.088534467 2.9927716e-09 0.02876661 -0.075311914 2.9927716e-09 0.054717332 -0.054717299
		 2.9927716e-09 0.075311951 -0.028766582 2.9927716e-09 0.088534474 7.1951685e-09 2.9927716e-09
		 0.093090661 0.0287666 2.9927716e-09 0.088534474 0.054717317 2.9927716e-09 0.075311929
		 0.075311922 2.9927716e-09 0.054717328 0.088534459 2.9927716e-09 0.028766604 0.093090639
		 2.9927716e-09 1.7347544e-08 -0.15624662 -5.8566099e-09 0.050767567 -0.13291134 -5.8566099e-09
		 0.096565679 -0.096565738 -5.8566099e-09 0.13291126 -0.050767619 -5.8566099e-09 0.15624657
		 -1.7647324e-08 -5.8566099e-09 0.16428736 0.050767571 -5.8566099e-09 0.15624657 0.096565671
		 -5.8566099e-09 0.13291124 0.13291124 -5.8566099e-09 0.096565664 0.15624657 -5.8566099e-09
		 0.050767552 0.16428734 -5.8566099e-09 -3.0592034e-08 0.15624657 -5.8566099e-09 -0.050767619
		 0.13291122 -5.8566099e-09 -0.096565694 0.096565664 -5.8566099e-09 -0.13291126 0.050767567
		 -5.8566099e-09 -0.15624659 -1.2751183e-08 -5.8566099e-09 -0.16428736 -0.050767582
		 -5.8566099e-09 -0.15624657 -0.096565671 -5.8566099e-09 -0.13291126 -0.13291124 -5.8566099e-09
		 -0.096565679 -0.15624657 -5.8566099e-09 -0.050767608 -0.16428734 -5.8566099e-09 -3.0592034e-08;
createNode polySplitRing -n "polySplitRing22";
	rename -uid "7DB49DDF-4862-E2C2-5FE5-51B84DA0B756";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[80:81]" "e[85]" "e[88]" "e[91]" "e[94]" "e[97]" "e[100]" "e[103]" "e[106]" "e[109]" "e[112]" "e[115]" "e[118]" "e[121]" "e[124]" "e[127]" "e[130]" "e[133]" "e[136]";
	setAttr ".ix" -type "matrix" 0.33621478516182851 -0.049091687299176547 0 0 0.027634492084624949 0.18926065349243507 -0.090080864129987503 0
		 0.020916900044944616 0.14325380612820085 0.30739421895277774 0 0.14023852756181071 1.6765711932058311 -0.2933694425771734 1;
	setAttr ".wt" 0.52911198139190674;
	setAttr ".dr" no;
	setAttr ".re" 127;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "0736C80D-4CF7-18BB-7E50-5DB77F742E38";
	setAttr ".ics" -type "componentList" 1 "f[160:162]";
	setAttr ".ix" -type "matrix" 0.98768834059513777 0 -0.15643446504023087 0 0 1.6999999752404025 0 0
		 0.15643446504023087 0 0.98768834059513777 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.0029399116 0.73854601 0.87667316 ;
	setAttr ".rs" 53409;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.45614488529409858 0.6039797605524535 0.79968024158489504 ;
	setAttr ".cbx" -type "double3" 0.45614478511526096 0.87311227242075962 0.99237540351061715 ;
createNode polyTweak -n "polyTweak14";
	rename -uid "0DE215FB-4439-A1EF-AF7C-FFBB4984D15F";
	setAttr ".uopa" yes;
	setAttr -s 32 ".tk[420:451]" -type "float3"  -0.077069141 0 0.36258185
		 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141
		 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185
		 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141 0 0.36258185 -0.077069141
		 0 0.36258185 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866
		 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866 -0.015333544
		 0 0.07213866 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866 -0.015333544 0 0.07213866
		 -0.096293129 -0.04218854 0.25619617 -0.096293129 -0.04218854 0.25619617 -0.096293129
		 -0.04218854 0.25619617 -0.096293129 -0.04218854 0.25619617 -0.096293129 -0.04218854
		 0.25619617 -0.096293129 -0.04218854 0.25619617 -0.096293129 -0.04218854 0.25619617
		 -0.096293129 -0.04218854 0.25619617 -0.096293129 -0.04218854 0.25619617 -0.096293129
		 -0.04218854 0.25619617;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "polyExtrudeFace3.out" "pCylinderShape1.i";
connectAttr "polySplitRing22.out" "pCylinderShape2.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing10.mp";
connectAttr "polyTweak1.out" "polySplitRing11.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing10.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polySplitRing12.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing12.mp";
connectAttr "polySplitRing11.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySplitRing13.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing12.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing14.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing13.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing15.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing15.mp";
connectAttr "polySplitRing14.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing16.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing16.mp";
connectAttr "polySplitRing15.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySplitRing17.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing17.mp";
connectAttr "polySplitRing16.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySplitRing18.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing18.mp";
connectAttr "polySplitRing17.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polySplitRing19.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing19.mp";
connectAttr "polySplitRing18.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polySplitRing19.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySplitRing20.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing20.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing21.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing21.mp";
connectAttr "polySplitRing20.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape2.wm" "polyExtrudeFace2.mp";
connectAttr "polyCylinder2.out" "polyTweak13.ip";
connectAttr "polyExtrudeFace2.out" "polySplitRing22.ip";
connectAttr "pCylinderShape2.wm" "polySplitRing22.mp";
connectAttr "polyTweak14.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing21.out" "polyTweak14.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
// End of Kawasaki_model_001.ma
